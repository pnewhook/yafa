﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Yafa.Domain
{
    public interface IEstablishment
    {
        int Id { get; set; }
        string Name { get; set; }
        string Address1 { get; set; }
        string Address2 { get; set; }
        string PhoneNumber { get; set; }
        double Latitude { get; set; }
        double Longitude { get; set; }
        string PostalCode { get; set; }
        ICity City { get; set; }
        ICountry Country { get; set; }
        IBrand Brand { get; set; }
    }
}
